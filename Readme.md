## rust-dev-container ##

This container allows the user to use it as a development environment for rust projects.


#### How to use ? ####

```
docker container create --name rustdev -v "$(PWD)/project:/home/project" sanketnaik/rust-dev-container
docker container start rustdev
```
The above command should create your containers and using the below command you can start the same. 

```
PLEASE NOTE: Prior to starting the container, please create the respective folder: $(PWD)/project
```

#### How to use  it with compose ? ####

```
version: '3'
volumes:
  rustvol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: $HOME/project

services:
  rustdev:
    image: sanketnaik/rust-dev-container
    volumes:
      - rustvol:/home/project
```
The above is a sample compose file. 

```
PLEASE NOTE: Prior to starting the containers, please create the respective folder: $HOME/project
```

#### Using for Developoment with vscode ####

Once the stack/container is up, then you can follow the instructions [here](https://code.visualstudio.com/docs/remote/containers#_attaching-to-running-containers) to attach Visual Studio Code and use this container as a development environment.
